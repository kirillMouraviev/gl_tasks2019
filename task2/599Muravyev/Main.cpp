#include <iostream>
#include <vector>

#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <fstream>

typedef std::pair<std::vector<MeshPtr>, TexturePtr> modelDefinitionType;

const char* wallsCoordsFile = "599MuravyevData2/wallsCoords.txt";
std::vector<std::pair<Point, Point> > walls;
std::vector<std::pair<Point, Point> > extendedWalls;

void readWalls(std::vector<std::pair<Point, Point> > &walls)
{
    std::ifstream fin(wallsCoordsFile);
    int n_walls;
    fin >> n_walls;
    for (int i = 0; i < n_walls; i++)
    {
        float x1, y1, x2, y2;
        fin >>  x1 >> y1 >> x2 >> y2;
        walls.push_back(std::make_pair(Point(x1, y1), Point(x2, y2)));
    }
}

class MyApplication: public Application
{
public:
    MeshPtr _floor;
    MeshPtr _walls;
    std::vector<modelDefinitionType> _models;

    MeshPtr _sunMarker; //Маркер для источника света
    LightInfo _sun;
    LightInfo _torch;
    float _torchBrightness;
    float _spotLightAngle;

    // Позиция источника света
    float _lr = 100.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    ShaderProgramPtr _objectShader;
    ShaderProgramPtr _sunShader;

    TexturePtr _floorTexture;
    TexturePtr _wallTexture;
    GLuint _sampler;


    modelDefinitionType createModel(const char* modelFilename,
                                    const char* textureFilename,
                                    const glm::vec3 coords,
                                    const glm::vec3 scale,
                                    const glm::vec3 rotationVector,
                                    const float rotationAngle
                                    )
    {
        std::vector<MeshPtr> meshes = loadManyMeshesFromFile(modelFilename);
        glm::mat4 source_matrix = glm::translate(glm::mat4(1.0f), coords);
        glm::mat4 scaled_matrix = glm::scale(source_matrix, scale);
        glm::mat4 rotated_matrix = glm::rotate(scaled_matrix, rotationAngle, rotationVector);
        for (auto mesh: meshes)
            mesh->setModelMatrix(rotated_matrix);
        TexturePtr texture = loadTexture(textureFilename);
        return std::make_pair(meshes, texture);
    }


    std::vector<modelDefinitionType> createModels()
    {
        std::vector<modelDefinitionType> result;
        // Our bunny in center of the maze
        result.push_back(createModel("599MuravyevData2/models/bunny.obj",
                                     "599MuravyevData2/images/white.jpg",
                                     glm::vec3(17.5f, 6.5f, 0.5f),
                                     glm::vec3(0.2f, 0.2f, 0.2f),
                                     glm::vec3(0.0f, 0.0f, 1.0f),
                                     0.0));
        // Our bunny in far corner
        result.push_back(createModel("599MuravyevData2/models/bunny.obj",
                                     "599MuravyevData2/images/white.jpg",
                                     glm::vec3(18.5f, 13.5f, 0.5f),
                                     glm::vec3(0.2f, 0.2f, 0.2f),
                                     glm::vec3(0.0f, 0.0f, 1.0f),
                                     0.0));
        // Our teapot in first corridor
        result.push_back(createModel("599MuravyevData2/models/teapot.obj",
                                     "599MuravyevData2/images/glass.jpg",
                                     glm::vec3(0.5f, 0.5f, 0.5f),
                                     glm::vec3(0.2f, 0.2f, 0.2f),
                                     glm::vec3(0.0f, 0.0f, 1.0f),
                                     glm::pi<float>() / 2.0f));
        // A toilet in far corner
        result.push_back(createModel("599MuravyevData2/models/Toilet/Toilet.obj",
                                     "599MuravyevData2/images/white.jpg",
                                     glm::vec3(0.5f, 13.5f, 0.75f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        // A toilet in another corner
        result.push_back(createModel("599MuravyevData2/models/Toilet/Toilet.obj",
                                     "599MuravyevData2/images/white.jpg",
                                     glm::vec3(21.0f, 5.0f, 0.75f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        // A skull near the entry
        result.push_back(createModel("599MuravyevData2/models/Skull/Skull.obj",
                                     "599MuravyevData2/models/Skull/Skull.jpg",
                                     glm::vec3(19.5f, 1.5f, 0.5f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     0.0f));
        result.push_back(createModel("599MuravyevData2/models/Skull/Skull.obj",
                                     "599MuravyevData2/models/Skull/Skull.jpg",
                                     glm::vec3(0.5f, 3.5f, 0.5f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     0.0f));
        result.push_back(createModel("599MuravyevData2/models/Skull/Skull.obj",
                                     "599MuravyevData2/models/Skull/Skull.jpg",
                                     glm::vec3(21.5f, 3.5f, 0.5f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     0.0f));
        result.push_back(createModel("599MuravyevData2/models/Skull/Skull.obj",
                                     "599MuravyevData2/models/Skull/Skull.jpg",
                                     glm::vec3(0.5f, 7.5f, 0.5f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     0.0f));
        // A plant near entry
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(20.0f, 1.5f, 0.5f),
                                     glm::vec3(0.05f, 0.05f, 0.05f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(1.0f, 9.5f, 0.5f),
                                     glm::vec3(0.1f, 0.1f, 0.1f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(7.0f, 9.5f, 0.5f),
                                     glm::vec3(0.1f, 0.1f, 0.1f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(21.0f, 13.0f, 0.5f),
                                     glm::vec3(0.1f, 0.1f, 0.1f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(15.0f, 9.0f, 0.5f),
                                     glm::vec3(0.1f, 0.1f, 0.1f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        return result;
    }


    void makeScene() override
    {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();

        //=========================================================
        //Создание и загрузка мешей
        _floor = makeGroundPlane(100.0, 100);
        _floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _walls = makeWalls(walls, 2.0);
        _walls->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _models = createModels();

        _sunMarker = makeSphere(1.0f);

        //=========================================================
        //Инициализация шейдеров

        _objectShader = std::make_shared<ShaderProgram>("599MuravyevData2/texture.vert", "599MuravyevData2/texture.frag");
        _sunShader = std::make_shared<ShaderProgram>("599MuravyevData2/marker.vert", "599MuravyevData2/marker.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        //"Солнце"
        _sun.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _sun.ambient = glm::vec3(0.1, 0.1, 0.1);
        _sun.diffuse = glm::vec3(0.0, 0.0, 0.0);
        _sun.specular = glm::vec3(0.0, 0.0, 0.0);
        //"Налобный фонарь"
        _torch.position = glm::vec3(0, 0, 0);
        _torch.ambient = glm::vec3(0.0, 0.0, 0.0);
        _torch.diffuse = glm::vec3(0.8, 0.5, 0.3);
        _torch.specular = glm::vec3(0.5, 0.5, 0.5);
        _torchBrightness = 5.0;
        _spotLightAngle = 0.9;

        //=========================================================
        //Загрузка и создание текстур
        _floorTexture = loadTexture("599MuravyevData2/images/mramor.jpg");
        _wallTexture = loadTexture("599MuravyevData2/images/stones.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Sun"))
            {
                ImGui::ColorEdit3("sun ambient", glm::value_ptr(_sun.ambient));
                ImGui::ColorEdit3("sun diffuse", glm::value_ptr(_sun.diffuse));
                ImGui::ColorEdit3("sun specular", glm::value_ptr(_sun.specular));

                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }

            if (ImGui::CollapsingHeader("Torch"))
            {
                ImGui::ColorEdit3("torch ambient", glm::value_ptr(_torch.ambient));
                ImGui::ColorEdit3("torch diffuse", glm::value_ptr(_torch.diffuse));
                ImGui::ColorEdit3("torch specular", glm::value_ptr(_torch.specular));
                ImGui::SliderFloat("brightness", &_torchBrightness, 0.0f, 50.0f);
                ImGui::SliderFloat("lighting angle (cos)", &_spotLightAngle, 0.0f, 1.0f);
            }
        }
        ImGui::End();
    }

    void update() override{

        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();

        _cameraMover->updateWithWalls(extendedWalls, _window, dt);
        _camera = _cameraMover->cameraInfo();
    }

    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _objectShader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _objectShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _objectShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _sun.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_sun.position, 1.0));
        glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_sun.position, 1.0));
        glm::vec3 viewDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_cameraMover->getViewDirection(), 0.0));

        _objectShader->setVec3Uniform("externalLight.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _objectShader->setVec3Uniform("externalLight.dir", lightDirCamSpace);
        _objectShader->setVec3Uniform("externalLight.La", _sun.ambient);
        _objectShader->setVec3Uniform("externalLight.Ld", _sun.diffuse);
        _objectShader->setVec3Uniform("externalLight.Ls", _sun.specular);

        _objectShader->setVec3Uniform("spotLight.La", _torch.ambient);
        _objectShader->setVec3Uniform("spotLight.Ld", _torch.diffuse);
        _objectShader->setVec3Uniform("spotLight.Ls", _torch.specular);
        _objectShader->setFloatUniform("spotLight.brightness", _torchBrightness);
        _objectShader->setVec3Uniform("spotLight.viewDirection", viewDirCamSpace);
        _objectShader->setFloatUniform("spotLight.lightingAngle", _spotLightAngle);
        glm::vec3 viewDir = _cameraMover->getViewDirection();

        GLuint textureUnit = 0;

        //Рисуем пол лабиринта
        if (USE_DSA) {
            glBindTextureUnit(textureUnit, _floorTexture->texture());
            glBindSampler(textureUnit, _sampler);
        }
        else {
            glBindSampler(textureUnit, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnit);  //текстурный юнит 0
            _floorTexture->bind();
        }
        _objectShader->setIntUniform("diffuseTex", textureUnit);

        {
            _objectShader->setMat4Uniform("modelMatrix", _floor->modelMatrix());
            _objectShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _floor->modelMatrix()))));

            _floor->draw();
        }

        //Рисуем стены лабиринта
        if (USE_DSA) {
            glBindTextureUnit(textureUnit, _wallTexture->texture());
            glBindSampler(textureUnit, _sampler);
        }
        else {
            glBindSampler(textureUnit, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnit);  //текстурный юнит 1
            _wallTexture->bind();
        }

        _objectShader->setMat4Uniform("modelMatrix", _walls->modelMatrix());
        _objectShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _walls->modelMatrix()))));
        _walls->draw();

        //Рисуем модели
        for (int i = 0; i < _models.size(); i++) {
            if (USE_DSA) {
                glBindTextureUnit(textureUnit, _models[i].second->texture());
                glBindSampler(textureUnit, _sampler);
            } else {
                glBindSampler(textureUnit, _sampler);
                glActiveTexture(GL_TEXTURE0 + textureUnit);  //текстурный юнит 1
                _models[i].second->bind();
            }
            for (int j = 0; j < _models[i].first.size(); j++) {
                _objectShader->setMat4Uniform("modelMatrix", _models[i].first[j]->modelMatrix());
                _objectShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _models[i].first[j]->modelMatrix()))));
                _models[i].first[j]->draw();
            }
        }

        //Рисуем "Солнце"
        {
            _sunShader->use();

            _sunShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix *
                                                       glm::translate(glm::mat4(1.0f), _sun.position));
            _sunShader->setVec4Uniform("color", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
            _sunMarker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    readWalls(walls);
    for (int i = 0; i < walls.size(); i++)
    {
        extendedWalls.push_back(walls[i]);
        float px = walls[i].first.x;
        float py = walls[i].first.y;
        float qx = walls[i].second.x;
        float qy = walls[i].second.y;
        if (px == qx)
        {
            extendedWalls.push_back(std::make_pair(Point(px + 0.1, py), Point(qx + 0.1, qy)));
            extendedWalls.push_back(std::make_pair(Point(px + 0.1, py), Point(px, py)));
            extendedWalls.push_back(std::make_pair(Point(qx + 0.1, qy), Point(qx, qy)));
        }
        else if (py == qy){
            extendedWalls.push_back(std::make_pair(Point(px, py + 0.1), Point(qx, qy + 0.1)));
            extendedWalls.push_back(std::make_pair(Point(px, py + 0.1), Point(px, py)));
            extendedWalls.push_back(std::make_pair(Point(qx, qy + 0.1), Point(qx, qy)));
        }
    }
    MyApplication app;
    app.start();
    return 0;
}