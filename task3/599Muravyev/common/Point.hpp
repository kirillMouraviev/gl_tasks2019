#pragma once

struct Point{
    float x;
    float y;
    float z;

    Point(float _x = 0, float _y = 0, float _z = 0): x(_x), y(_y), z(_z) {};
};