#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <QueryObject.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtx/transform.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <deque>

typedef std::pair<std::vector<MeshPtr>, TexturePtr> modelDefinitionType;

const char* wallsCoordsFile = "599MuravyevData3/wallsCoords.txt";
std::vector<std::pair<Point, Point> > walls;
std::vector<std::pair<Point, Point> > extendedWalls;

class QueryResultProcessor {
public:
    virtual ~QueryResultProcessor() { }
    virtual double getValue() const = 0;
    virtual void clear(double value) = 0;
    virtual void pushResult(double value) = 0;
};

class HistoryAverager : public QueryResultProcessor {
public:
    HistoryAverager(size_t N) {
        results = std::vector<double>(N);
        clear(0.0);
    }

    double getValue() const override {
        if (changed) {
            double sum = 0.0;
            for (size_t i = 0; i < results.size(); i++)
                sum += results[i];
            avg = sum / results.size();
            changed = false;
        }
        return avg;
    }

    void clear(double result) override {
        for (size_t i = 0; i < results.size(); i++)
            results[i] = result;
        changed = true;
    }

    void pushResult(double result) override {
        results[index] = result;
        ++index;
        if (index == results.size())
            index = 0;
        changed = true;
    }
protected:
    std::vector<double> results;
    mutable double avg = 0.0;
    mutable bool changed = true;
    int index = 0;
};

class QueryCounter {
private:
    void subscribeForQueryManager() {
        queryManager.setQueryResultHandler(std::bind(&QueryCounter::onQueryResultReady, this, std::placeholders::_1));
    }
public:
    void onQueryResultReady(const QueryObjectPtr &occlusionQuery) {
        history->pushResult(static_cast<double>(occlusionQuery->getResultSync()));
    }

    explicit QueryCounter(std::shared_ptr<QueryResultProcessor> _history, QueryObject::Target target) : history(
            std::move(_history)), queryManager(target) {
        // If we put 1 here, the number of lost queries will grow every frame.
        queryManager.setMaxPendingQueries(2);
        subscribeForQueryManager();
    }
    QueryCounter(const QueryCounter &) = delete;
    QueryCounter &operator=(const QueryCounter) = delete;

    QueryCounter(QueryCounter &&qc) : history(std::move(qc.history)), queryManager(std::move(qc.queryManager)) {
        subscribeForQueryManager();
    }

    QueryCounter &operator=(QueryCounter &&qc) {
        queryManager.setQueryResultHandler(nullptr);
        history = std::move(qc.history);
        queryManager = std::move(qc.queryManager);
        subscribeForQueryManager();
        return *this;
    }

    double getValue() {
        return history->getValue();
    }

    void clearHistory() {
        history->clear(0.0);
    }

    QueryManager &getQueryManager() { return queryManager; }
    const QueryManager &getQueryManager() const { return queryManager; }
protected:
    std::shared_ptr<QueryResultProcessor> history;
    QueryManager queryManager;
};

void readWalls(std::vector<std::pair<Point, Point> > &walls)
{
    std::ifstream fin(wallsCoordsFile);
    int n_walls;
    fin >> n_walls;
    for (int i = 0; i < n_walls; i++)
    {
        float x1, y1, x2, y2;
        fin >>  x1 >> y1 >> x2 >> y2;
        walls.push_back(std::make_pair(Point(x1, y1), Point(x2, y2)));
    }
}

class MyApplication: public Application
{
public:
    MeshPtr _floor;
    MeshPtr _walls;
    std::vector<modelDefinitionType> _models;

    MeshPtr _sunMarker; //Маркер для источника света
    LightInfo _sun;
    LightInfo _torch;
    float _torchBrightness;
    float _spotLightAngle;

    // Позиция источника света
    float _lr = 100.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    ShaderProgramPtr _objectShader;
    ShaderProgramPtr _instancingShader;
    ShaderProgramPtr _occlusionShader;
    ShaderProgramPtr _sunShader;

    TexturePtr _floorTexture;
    TexturePtr _wallTexture;
    GLuint _sampler;
    QueryCounter timerQuery = QueryCounter(std::make_shared<HistoryAverager>(120), QueryObject::QOT_TIME_ELAPSED);

    std::vector<glm::vec3> positions; // Позиции сфер.
    std::vector<QueryManager> queryPools_conservative;
    std::vector<QueryManager> queryPools;
    std::vector<QueryObjectPtr> currentQueries;

    bool conditionalRendering = false; // Условный рендеринг.
    bool conservativeCount = false; // GL_ANY_SAMPLES_PASSED_CONSERVATIVE vs GL_ANY_SAMPLES_PASSED
    bool occQueryWait = false; // входит в параметр условного рендеринга.
    bool occQueryByRegion = false; // входит в параметр условного рендеринга.


    modelDefinitionType createModel(const char* modelFilename,
                                    const char* textureFilename,
                                    const glm::vec3 coords,
                                    const glm::vec3 scale,
                                    const glm::vec3 rotationVector,
                                    const float rotationAngle
                                    )
    {
        std::vector<MeshPtr> meshes = loadManyMeshesFromFile(modelFilename);
        glm::mat4 source_matrix = glm::translate(glm::mat4(1.0f), coords);
        glm::mat4 scaled_matrix = glm::scale(source_matrix, scale);
        glm::mat4 rotated_matrix = glm::rotate(scaled_matrix, rotationAngle, rotationVector);
        for (auto mesh: meshes)
            mesh->setModelMatrix(rotated_matrix);
        TexturePtr texture = loadTexture(textureFilename);
        return std::make_pair(meshes, texture);
    }


    modelDefinitionType createModelArray(const char* modelFilename,
                                        const char* textureFilename,
                                        std::vector<glm::vec3> positions,
                                        const glm::vec3 scale,
                                        const glm::vec3 rotationVector,
                                        const float rotationAngle
    )
    {
        std::vector<MeshPtr> meshes = loadManyMeshesFromFileArray(modelFilename, positions);
        glm::mat4 source_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
        glm::mat4 scaled_matrix = glm::scale(source_matrix, scale);
        glm::mat4 rotated_matrix = glm::rotate(scaled_matrix, rotationAngle, rotationVector);
        for (auto mesh: meshes)
            mesh->setModelMatrix(rotated_matrix);
        TexturePtr texture = loadTexture(textureFilename);
        return std::make_pair(meshes, texture);
    }


    std::vector<modelDefinitionType> createModels()
    {
        std::vector<modelDefinitionType> result;
        positions = {glm::vec3(17.5f, 6.5f, 1.5f),
                     glm::vec3(18.5f, 12.5f, 1.5f),
                     glm::vec3(17.5f, 12.5f, 1.5f),
                     glm::vec3(16.5f, 12.5f, 1.5f),
                     glm::vec3(15.5f, 12.5f, 1.5f),
                     glm::vec3(14.5f, 12.5f, 1.5f),
                     glm::vec3(100.0f, 100.0f, 1.5f),
                     };
        // Our bunny in center of the maze
        result.push_back(createModelArray("599MuravyevData2/models/bunny.obj",
                                         "599MuravyevData2/images/white.jpg",
                                         positions,
                                         glm::vec3(0.2f, 0.2f, 0.2f),
                                         glm::vec3(0.0f, 0.0f, 1.0f),
                                         0.0));
        // Our teapot in first corridor
        /*result.push_back(createModel("599MuravyevData2/models/teapot.obj",
                                     "599MuravyevData2/images/glass.jpg",
                                     glm::vec3(0.5f, 0.5f, 0.5f),
                                     glm::vec3(0.2f, 0.2f, 0.2f),
                                     glm::vec3(0.0f, 0.0f, 1.0f),
                                     glm::pi<float>() / 2.0f));*/
        // A toilet in far corner
        /*result.push_back(createModel("599MuravyevData2/models/Toilet/Toilet.obj",
                                     "599MuravyevData2/images/white.jpg",
                                     glm::vec3(0.5f, 13.5f, 0.75f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        // A toilet in another corner
        result.push_back(createModel("599MuravyevData2/models/Toilet/Toilet.obj",
                                     "599MuravyevData2/images/white.jpg",
                                     glm::vec3(21.0f, 5.0f, 0.75f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        // A skull near the entry
        result.push_back(createModel("599MuravyevData2/models/Skull/Skull.obj",
                                     "599MuravyevData2/models/Skull/Skull.jpg",
                                     glm::vec3(19.5f, 1.5f, 0.5f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     0.0f));
        result.push_back(createModel("599MuravyevData2/models/Skull/Skull.obj",
                                     "599MuravyevData2/models/Skull/Skull.jpg",
                                     glm::vec3(0.5f, 3.5f, 0.5f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     0.0f));
        result.push_back(createModel("599MuravyevData2/models/Skull/Skull.obj",
                                     "599MuravyevData2/models/Skull/Skull.jpg",
                                     glm::vec3(21.5f, 3.5f, 0.5f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     0.0f));
        result.push_back(createModel("599MuravyevData2/models/Skull/Skull.obj",
                                     "599MuravyevData2/models/Skull/Skull.jpg",
                                     glm::vec3(0.5f, 7.5f, 0.5f),
                                     glm::vec3(0.005f, 0.005f, 0.005f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     0.0f));
        // A plant near entry
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(20.0f, 1.5f, 0.5f),
                                     glm::vec3(0.05f, 0.05f, 0.05f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(1.0f, 9.5f, 0.5f),
                                     glm::vec3(0.1f, 0.1f, 0.1f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(7.0f, 9.5f, 0.5f),
                                     glm::vec3(0.1f, 0.1f, 0.1f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(21.0f, 13.0f, 0.5f),
                                     glm::vec3(0.1f, 0.1f, 0.1f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));
        result.push_back(createModel("599MuravyevData2/models/IndoorPotPlant/indoor_plant.obj",
                                     "599MuravyevData2/models/IndoorPotPlant/indoor_plant.jpg",
                                     glm::vec3(15.0f, 9.0f, 0.5f),
                                     glm::vec3(0.1f, 0.1f, 0.1f),
                                     glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::pi<float>() / 2.0f));*/
        return result;
    }


    void makeScene() override
    {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();

        //=========================================================
        //Создание и загрузка мешей
        _floor = makeGroundPlane(100.0, 100);
        _floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _walls = makeWalls(walls, 2.0);
        _walls->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _models = createModels();

        _sunMarker = makeSphere(1.0f);

        for (int i = 0; i < positions.size(); i++)
        {
            queryPools_conservative.push_back(QueryManager(QueryObject::QOT_ANY_SAMPLES_PASSED_CONSERVATIVE));
            queryPools.push_back(QueryManager(QueryObject::QOT_ANY_SAMPLES_PASSED));

            // 2 запроса в пуле, т.к. к следующему кадру не все успевают освободиться.
            queryPools[i].setMaxPendingQueries(2);
            queryPools_conservative[i].setMaxPendingQueries(2);
        }
        currentQueries.resize(positions.size());

        //=========================================================
        //Инициализация шейдеров

        _objectShader = std::make_shared<ShaderProgram>("599MuravyevData3/texture.vert", "599MuravyevData3/texture.frag");
        _instancingShader = std::make_shared<ShaderProgram>("599MuravyevData3/instancingUniform.vert", "599MuravyevData3/texture.frag");
        _occlusionShader = std::make_shared<ShaderProgram>("599MuravyevData3/occquery.vert", "599MuravyevData3/occquery.frag");
        _sunShader = std::make_shared<ShaderProgram>("599MuravyevData3/marker.vert", "599MuravyevData3/marker.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        //"Солнце"
        _sun.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _sun.ambient = glm::vec3(0.1, 0.1, 0.1);
        _sun.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _sun.specular = glm::vec3(0.0, 0.0, 0.0);
        //"Налобный фонарь"
        _torch.position = glm::vec3(0, 0, 0);
        _torch.ambient = glm::vec3(0.0, 0.0, 0.0);
        _torch.diffuse = glm::vec3(0.8, 0.5, 0.3);
        _torch.specular = glm::vec3(0.5, 0.5, 0.5);
        _torchBrightness = 5.0;
        _spotLightAngle = 0.9;

        //=========================================================
        //Загрузка и создание текстур
        _floorTexture = loadTexture("599MuravyevData2/images/mramor.jpg");
        _wallTexture = loadTexture("599MuravyevData2/images/stones.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Sun")) {
                ImGui::ColorEdit3("sun ambient", glm::value_ptr(_sun.ambient));
                ImGui::ColorEdit3("sun diffuse", glm::value_ptr(_sun.diffuse));
                ImGui::ColorEdit3("sun specular", glm::value_ptr(_sun.specular));

                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }

            if (ImGui::CollapsingHeader("Torch")) {
                ImGui::ColorEdit3("torch ambient", glm::value_ptr(_torch.ambient));
                ImGui::ColorEdit3("torch diffuse", glm::value_ptr(_torch.diffuse));
                ImGui::ColorEdit3("torch specular", glm::value_ptr(_torch.specular));
                ImGui::SliderFloat("brightness", &_torchBrightness, 0.0f, 50.0f);
                ImGui::SliderFloat("lighting angle (cos)", &_spotLightAngle, 0.0f, 1.0f);
            }

            ImGui::Checkbox("Conditional rendering", &conditionalRendering);
            if (conditionalRendering) {
                if (ImGui::CollapsingHeader("Conditional render", ImGuiTreeNodeFlags_DefaultOpen)) {
                    ImGui::Checkbox("Conservative count", &conservativeCount);
                    ImGui::Checkbox("Wait for results", &occQueryWait);
                    ImGui::Checkbox("Clip by region", &occQueryByRegion);
                }
            }

            timerQuery.getQueryManager().processFinishedQueries();
            ImGui::LabelText("Timer, ms", "%.0f", timerQuery.getValue() * 1e-3);
        }
        ImGui::End();
    }

    void update() override{

        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();

        _cameraMover->updateWithWalls(extendedWalls, _window, dt);
        _camera = _cameraMover->cameraInfo();
    }

    void draw() override {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        auto query = timerQuery.getQueryManager().beginQuery();

        //Подключаем шейдер
        _objectShader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _objectShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _objectShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _sun.position =
                glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_sun.position, 1.0));
        glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_sun.position, 1.0));
        glm::vec3 viewDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_cameraMover->getViewDirection(), 0.0));

        _objectShader->setVec3Uniform("externalLight.pos",
                                      lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _objectShader->setVec3Uniform("externalLight.dir", lightDirCamSpace);
        _objectShader->setVec3Uniform("externalLight.La", _sun.ambient);
        _objectShader->setVec3Uniform("externalLight.Ld", _sun.diffuse);
        _objectShader->setVec3Uniform("externalLight.Ls", _sun.specular);

        _objectShader->setVec3Uniform("spotLight.La", _torch.ambient);
        _objectShader->setVec3Uniform("spotLight.Ld", _torch.diffuse);
        _objectShader->setVec3Uniform("spotLight.Ls", _torch.specular);
        _objectShader->setFloatUniform("spotLight.brightness", _torchBrightness);
        _objectShader->setVec3Uniform("spotLight.viewDirection", viewDirCamSpace);
        _objectShader->setFloatUniform("spotLight.lightingAngle", _spotLightAngle);

        _instancingShader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _instancingShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _instancingShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _instancingShader->setVec3Uniform("externalLight.pos",
                                          lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _instancingShader->setVec3Uniform("externalLight.dir", lightDirCamSpace);
        _instancingShader->setVec3Uniform("externalLight.La", _sun.ambient);
        _instancingShader->setVec3Uniform("externalLight.Ld", _sun.diffuse);
        _instancingShader->setVec3Uniform("externalLight.Ls", _sun.specular);

        _instancingShader->setVec3Uniform("spotLight.La", _torch.ambient);
        _instancingShader->setVec3Uniform("spotLight.Ld", _torch.diffuse);
        _instancingShader->setVec3Uniform("spotLight.Ls", _torch.specular);
        _instancingShader->setFloatUniform("spotLight.brightness", _torchBrightness);
        _instancingShader->setVec3Uniform("spotLight.viewDirection", viewDirCamSpace);
        _instancingShader->setFloatUniform("spotLight.lightingAngle", _spotLightAngle);


        glm::vec3 viewDir = _cameraMover->getViewDirection();

        GLuint textureUnit = 0;

        //Рисуем пол лабиринта
        if (USE_DSA) {
            glBindTextureUnit(textureUnit, _floorTexture->texture());
            glBindSampler(textureUnit, _sampler);
        } else {
            glBindSampler(textureUnit, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnit);  //текстурный юнит 0
            _floorTexture->bind();
        }
        _objectShader->setIntUniform("diffuseTex", textureUnit);

        {
            _objectShader->use();
            _objectShader->setMat4Uniform("modelMatrix", _floor->modelMatrix());
            _objectShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                    glm::inverse(glm::mat3(_camera.viewMatrix * _floor->modelMatrix()))));

            _floor->draw();
        }

        //Рисуем стены лабиринта
        if (USE_DSA) {
            glBindTextureUnit(textureUnit, _wallTexture->texture());
            glBindSampler(textureUnit, _sampler);
        } else {
            glBindSampler(textureUnit, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnit);  //текстурный юнит 1
            _wallTexture->bind();
        }

        _objectShader->use();
        _objectShader->setMat4Uniform("modelMatrix", _walls->modelMatrix());
        _objectShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                glm::inverse(glm::mat3(_camera.viewMatrix * _walls->modelMatrix()))));
        _walls->draw();

        //Рисуем модели
        if (conditionalRendering)
        {
            conditionalPrepass(currentQueries);
            for (int i = 0; i < 1; i++)
            {
                std::cout << "iii: " << i << std::endl;
                ConditionalRender* conditionalRender = nullptr;
                if (currentQueries[i]) {
                    // Начинаем условный рендеринг.
                    conditionalRender = currentQueries[i]->beginConditionalRender(
                            occQueryWait ? (occQueryByRegion ? (ConditionalRender::WM_QUERY_BY_REGION_WAIT)
                                                             : (ConditionalRender::WM_QUERY_WAIT)) : (occQueryByRegion
                                                                                                      ? (ConditionalRender::WM_QUERY_BY_REGION_NO_WAIT)
                                                                                                      : (ConditionalRender::WM_QUERY_NO_WAIT)));
                }
                if (USE_DSA) {
                    glBindTextureUnit(textureUnit, _models[i].second->texture());
                    glBindSampler(textureUnit, _sampler);
                } else {
                    glBindSampler(textureUnit, _sampler);
                    glActiveTexture(GL_TEXTURE0 + textureUnit);  //текстурный юнит 1
                    _models[i].second->bind();
                }
                for (int j = 0; j < _models[i].first.size(); j++) {
                    //_objectShader->use();
                    //_objectShader->setMat4Uniform("modelMatrix", _models[i].first[j]->modelMatrix());
                    //_objectShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _models[i].first[j]->modelMatrix()))));
                    _instancingShader->use();
                    _instancingShader->setVec3Uniforms("positions", positions);
                    _instancingShader->setMat4Uniform("modelMatrix", _models[i].first[j]->modelMatrix());
                    _instancingShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                            glm::inverse(glm::mat3(_camera.viewMatrix * _models[i].first[j]->modelMatrix()))));
                    _models[i].first[j]->draw();
                }
                std::cout << "drawn" << std::endl;
                conditionalRender->endConditionalRender();
            }
            for (int i = 0; i < positions.size(); i++) {
                std::cout << "ii: " << i << std::endl;
                queryPools[i].processFinishedQueries();
                queryPools_conservative[i].processFinishedQueries();
            }
            std::cout << "finished" << std::endl;
        }

        //Рисуем "Солнце"
        {
            _sunShader->use();

            _sunShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix *
                                                    glm::translate(glm::mat4(1.0f), _sun.position));
            _sunShader->setVec4Uniform("color", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
            _sunMarker->draw();
        }
        if (query)
            query->endQuery();

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void conditionalPrepass(std::vector<QueryObjectPtr> &queries) {
        _occlusionShader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _occlusionShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _occlusionShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        // Отключаем запись в буфер цвета и глубины. Нам всего лишь нужно пройти тест глубины (даст запись в результат запроса).
        glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
        glDepthMask(GL_FALSE);
        for (size_t i = 0; i < positions.size(); i++) {
            std::cout << "i: " << i << std::endl;
            // Стартуем запрос с одним из двух способов установления факта прохождения тестов фрагментами.
            auto query = (conservativeCount ? queryPools_conservative[i].beginQuery() : queryPools[i].beginQuery());
            queries[i] = query;
            if (query) {
                glm::mat4 modelMatrix = glm::translate(positions[i]);
                _occlusionShader->setMat4Uniform("modelMatrix", modelMatrix);

                _models[0].first[0]->draw();
                query->endQuery();
            }
        }
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(GL_TRUE);
    }
};

int main()
{
    readWalls(walls);
    for (int i = 0; i < walls.size(); i++)
    {
        extendedWalls.push_back(walls[i]);
        float px = walls[i].first.x;
        float py = walls[i].first.y;
        float qx = walls[i].second.x;
        float qy = walls[i].second.y;
        if (px == qx)
        {
            extendedWalls.push_back(std::make_pair(Point(px + 0.1, py), Point(qx + 0.1, qy)));
            extendedWalls.push_back(std::make_pair(Point(px + 0.1, py), Point(px, py)));
            extendedWalls.push_back(std::make_pair(Point(qx + 0.1, qy), Point(qx, qy)));
        }
        else if (py == qy){
            extendedWalls.push_back(std::make_pair(Point(px, py + 0.1), Point(qx, qy + 0.1)));
            extendedWalls.push_back(std::make_pair(Point(px, py + 0.1), Point(px, py)));
            extendedWalls.push_back(std::make_pair(Point(qx, qy + 0.1), Point(qx, qy)));
        }
    }
    MyApplication app;
    app.start();
    return 0;
}