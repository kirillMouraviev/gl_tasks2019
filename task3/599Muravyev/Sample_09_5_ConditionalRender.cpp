#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <QueryObject.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtx/transform.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include <deque>

namespace {
float frand() {
	return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}
}


class QueryResultProcessor {
public:
	virtual ~QueryResultProcessor() { }
	virtual double getValue() const = 0;
	virtual void clear(double value) = 0;
	virtual void pushResult(double value) = 0;
};

class HistoryAverager : public QueryResultProcessor {
public:
	HistoryAverager(size_t N) {
		results = std::vector<double>(N);
		clear(0.0);
	}

	double getValue() const override {
		if (changed) {
			double sum = 0.0;
			for (size_t i = 0; i < results.size(); i++)
				sum += results[i];
			avg = sum / results.size();
			changed = false;
		}
		return avg;
	}

	void clear(double result) override {
		for (size_t i = 0; i < results.size(); i++)
			results[i] = result;
		changed = true;
	}

	void pushResult(double result) override {
		results[index] = result;
		++index;
		if (index == results.size())
			index = 0;
		changed = true;
	}
protected:
	std::vector<double> results;
	mutable double avg = 0.0;
	mutable bool changed = true;
	int index = 0;
};

class QueryCounter {
private:
	void subscribeForQueryManager() {
		queryManager.setQueryResultHandler(std::bind(&QueryCounter::onQueryResultReady, this, std::placeholders::_1));
	}
public:
	void onQueryResultReady(const QueryObjectPtr &occlusionQuery) {
		history->pushResult(static_cast<double>(occlusionQuery->getResultSync()));
	}

	explicit QueryCounter(std::shared_ptr<QueryResultProcessor> _history, QueryObject::Target target) : history(
			std::move(_history)), queryManager(target) {
		// If we put 1 here, the number of lost queries will grow every frame.
		queryManager.setMaxPendingQueries(2);
		subscribeForQueryManager();
	}
	QueryCounter(const QueryCounter &) = delete;
	QueryCounter &operator=(const QueryCounter) = delete;

	QueryCounter(QueryCounter &&qc) : history(std::move(qc.history)), queryManager(std::move(qc.queryManager)) {
		subscribeForQueryManager();
	}

	QueryCounter &operator=(QueryCounter &&qc) {
		queryManager.setQueryResultHandler(nullptr);
		history = std::move(qc.history);
		queryManager = std::move(qc.queryManager);
		subscribeForQueryManager();
		return *this;
	}

	double getValue() {
		return history->getValue();
	}

	void clearHistory() {
		history->clear(0.0);
	}

	QueryManager &getQueryManager() { return queryManager; }
	const QueryManager &getQueryManager() const { return queryManager; }
protected:
	std::shared_ptr<QueryResultProcessor> history;
	QueryManager queryManager;
};

/**
Условный рендеринг.
*/
class SampleApplication : public Application
{
public:
	MeshPtr _sphere;
	MeshPtr _cube;
	MeshPtr _plane;

	TexturePtr _brickTex;
	GLuint _sampler;

	// Timer query
	QueryCounter timerQuery = QueryCounter(std::make_shared<HistoryAverager>(120), QueryObject::QOT_TIME_ELAPSED);

	std::vector<glm::vec3> positions; // Позиции сфер.
	std::vector<QueryManager> queryPools_conservative;
	std::vector<QueryManager> queryPools;
	std::vector<QueryObjectPtr> currentQueries;

	bool conditionalRendering = false; // Условный рендеринг.
	bool conservativeCount = false; // GL_ANY_SAMPLES_PASSED_CONSERVATIVE vs GL_ANY_SAMPLES_PASSED
	bool occQueryWait = false; // входит в параметр условного рендеринга.
	bool occQueryByRegion = false; // входит в параметр условного рендеринга.

	//------------------------

	ShaderProgramPtr _occlusionShader;
	ShaderProgramPtr _commonShader;


	//------------------------

	//Переменные для управления положением одного источника света
	float _lr = 10.0;
	float _phi = 0.0;
	float _theta = 0.48;

	LightInfo _light;

	//------------------------

	const unsigned int K = 1000; //Количество инстансов


	void makeScene() override
	{
		Application::makeScene();

		//=========================================================
		//Инициализируем K случайных сдвигов для K экземпляров
		const float size = 50.0f;
		for (unsigned int i = 0; i < K; i++)
		{
			positions.push_back(glm::vec3(frand() * size - 0.5 * size, frand() * size - 0.5 * size, 0.0));
			queryPools_conservative.push_back(QueryManager(QueryObject::QOT_ANY_SAMPLES_PASSED_CONSERVATIVE));
			queryPools.push_back(QueryManager(QueryObject::QOT_ANY_SAMPLES_PASSED));

			// 2 запроса в пуле, т.к. к следующему кадру не все успевают освободиться.
			queryPools[i].setMaxPendingQueries(2);
			queryPools_conservative[i].setMaxPendingQueries(2);
		}
		currentQueries.resize(K);

		//----------------------------

		_sphere = makeSphere(0.5f);
		_cube = makeCube(0.5f);
		_plane = makeGroundPlane(100, 100);
		_plane->setModelMatrix(glm::translate(glm::mat4(1), glm::vec3(0, 0, 1)));

		//----------------------------

		//=========================================================
		//Инициализация шейдеров
		_commonShader = std::make_shared<ShaderProgram>("shaders/common.vert", "shaders/common.frag");
		_occlusionShader = std::make_shared<ShaderProgram>("shaders9/occquery.vert", "shaders9/occquery.frag");

		//=========================================================
		//Инициализация значений переменных освщения
		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_light.ambient = glm::vec3(0.2, 0.2, 0.2);
		_light.diffuse = glm::vec3(0.8, 0.8, 0.8);
		_light.specular = glm::vec3(1.0, 1.0, 1.0);

		//=========================================================
		//Загрузка и создание текстур
		_brickTex = loadTexture("images/brick.jpg");

		//=========================================================
		//Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
				ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
				ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

				ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}

			ImGui::Checkbox("Conditional rendering", &conditionalRendering);
			if (conditionalRendering) {
				if (ImGui::CollapsingHeader("Conditional render", ImGuiTreeNodeFlags_DefaultOpen)) {
					ImGui::Checkbox("Conservative count", &conservativeCount);
					ImGui::Checkbox("Wait for results", &occQueryWait);
					ImGui::Checkbox("Clip by region", &occQueryByRegion);
				}
			}

			timerQuery.getQueryManager().processFinishedQueries();
			ImGui::LabelText("Timer, ms", "%.0f", timerQuery.getValue() * 1e-3);
		}
		ImGui::End();
	}

	void handleKey(int key, int scancode, int action, int mods) override
	{
		Application::handleKey(key, scancode, action, mods);

	}

	void update()
	{
		Application::update();

		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
	}

	void draw() override
	{
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		auto query = timerQuery.getQueryManager().beginQuery();
		drawScene();
		if (query)
			query->endQuery();

		//Отсоединяем сэмплер и шейдерную программу
		glBindSampler(0, 0);
		glUseProgram(0);
	}

	void drawStandard() {
		for (unsigned int i = 0; i < positions.size(); i++)
		{
			glm::mat4 modelMatrix = glm::translate(positions[i]);

			_commonShader->setMat4Uniform("modelMatrix", modelMatrix);
			_commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * modelMatrix))));

			_sphere->draw();
		}
	}

	void drawConditional() {
		for (unsigned int i = 0; i < positions.size(); i++)
		{
			ConditionalRender* conditionalRender = nullptr;
			if (currentQueries[i]) {
				// Начинаем условный рендеринг.
				conditionalRender = currentQueries[i]->beginConditionalRender(
						occQueryWait ? (occQueryByRegion ? (ConditionalRender::WM_QUERY_BY_REGION_WAIT)
								: (ConditionalRender::WM_QUERY_WAIT)) : (occQueryByRegion
								? (ConditionalRender::WM_QUERY_BY_REGION_NO_WAIT)
								: (ConditionalRender::WM_QUERY_NO_WAIT)));
			}
			glm::mat4 modelMatrix = glm::translate(positions[i]);

			_commonShader->setMat4Uniform("modelMatrix", modelMatrix);
			_commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * modelMatrix))));

			_sphere->draw();
			if (conditionalRender) {
				conditionalRender->endConditionalRender();
			}
		}
	}

	void conditionalPrepass(std::vector<QueryObjectPtr> &queries) {
		_occlusionShader->use();

		//Загружаем на видеокарту значения юниформ-переменных
		_occlusionShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_occlusionShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		// Отключаем запись в буфер цвета и глубины. Нам всего лишь нужно пройти тест глубины (даст запись в результат запроса).
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glDepthMask(GL_FALSE);
		for (size_t i = 0; i < positions.size(); i++) {
			// Стартуем запрос с одним из двух способов установления факта прохождения тестов фрагментами.
			auto query = (conservativeCount ? queryPools_conservative[i].beginQuery() : queryPools[i].beginQuery());
			queries[i] = query;
			if (query) {
				glm::mat4 modelMatrix = glm::translate(positions[i]);
				_occlusionShader->setMat4Uniform("modelMatrix", modelMatrix);

				_cube->draw();
				query->endQuery();
			}
		}
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glDepthMask(GL_TRUE);
	}

	void drawScene()
	{
		_commonShader->use();

		//Загружаем на видеокарту значения юниформ-переменных
		_commonShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_commonShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
		_commonShader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
		_commonShader->setVec3Uniform("light.La", _light.ambient);
		_commonShader->setVec3Uniform("light.Ld", _light.diffuse);
		_commonShader->setVec3Uniform("light.Ls", _light.specular);

		glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
		glBindSampler(0, _sampler);
		_brickTex->bind();
		_commonShader->setIntUniform("diffuseTex", 0);

		glm::mat4 modelMatrix = _plane->modelMatrix();
		_commonShader->setMat4Uniform("modelMatrix", modelMatrix);
		_commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * modelMatrix))));
		_plane->draw();

		if (conditionalRendering) {
			// Сначала порендерим bounding boxes без записи в цвет и буфер глубины.
			conditionalPrepass(currentQueries);
			_commonShader->use();
		}

		if (conditionalRendering) {
			drawConditional();
		}
		else {
			drawStandard();
		}

		if (conditionalRendering) {
			// Чисто для пулов запросов: помечаем выполнившиеся запросы как свободные для использования.
			for (int i = 0; i < positions.size(); i++) {
				queryPools[i].processFinishedQueries();
				queryPools_conservative[i].processFinishedQueries();
			}
		}
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}