/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;

struct DirectionalLightInfo
{
	vec3 dir; //направление от источника света (для направленного освещения) в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};

struct SpotLightInfo
{
	vec3 viewDirection; //направление взгляда (куда смотрит камера), для "налобного фонаря", в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
	float brightness; //яркость (интенсивность освещения)
	float lightingAngle; //угол освещения - минимальный косинус угла между направлением взгляда и нормалью, на который попадает свет
};

uniform DirectionalLightInfo externalLight;
uniform SpotLightInfo spotLight;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

void main()
{
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;

	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 directionToCamera = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	
	vec3 lightDirCamSpace = normalize(externalLight.dir); //направление на внешний источник света
	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0);
	vec3 externalLightColor = diffuseColor * (externalLight.La + externalLight.Ld * NdotL);

	vec3 color = externalLightColor;

	if (NdotL > 0.0)
    {
        vec3 halfVector = normalize(lightDirCamSpace.xyz + directionToCamera); //биссектриса между направлениями на камеру и на источник света

        float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
        color += externalLight.Ls * Ks * blinnTerm;
    }

	lightDirCamSpace = normalize(-posCamSpace.xyz); //направление на "налобный фонарь" (на камеру)
	float angle = max(dot(-lightDirCamSpace.xyz, normalize(spotLight.viewDirection)), 0); //косинус угла между направлением на камеру и направлением взгляда камеры
	if (angle > spotLight.lightingAngle)
	{
        float distanceToCamera = sqrt(dot(posCamSpace.xyz, posCamSpace.xyz));
        NdotL = max(dot(normal, lightDirCamSpace.xyz), -dot(normal, lightDirCamSpace.xyz)); //скалярное произведение (косинус)
        vec3 spotLightColor = diffuseColor * (spotLight.La + spotLight.Ld * NdotL) * spotLight.brightness / distanceToCamera;
        float angleCoef = (angle - spotLight.lightingAngle) / (1.0 - spotLight.lightingAngle);
        color += spotLightColor * angleCoef * angleCoef;

        if (NdotL > 0.0)
        {
            vec3 halfVector = normalize(lightDirCamSpace.xyz); //биссектриса между направлениями на камеру и на источник света - в данном случае направление на камеру

            float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
            blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
            blinnTerm *= spotLight.brightness / distanceToCamera;
            color += spotLight.Ls * Ks * blinnTerm * angleCoef * angleCoef;
        }
	}

	fragColor = vec4(color, 1.0);
}
